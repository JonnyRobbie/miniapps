# Mini applications

[https://jonnyrobbie.gitlab.io/miniapps](https://jonnyrobbie.gitlab.io/miniapps)

A collection of single-page static mini applications for various purposes.

* [Visual center](https://jonnyrobbie.gitlab.io/miniapps/visualcenter.html) Finding visual center in the frame according to Trangent's guide from [this video](https://youtu.be/MyWImlYBvyA).

